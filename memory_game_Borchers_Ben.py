# Projektarbeit von Ben Borchers 
# cimdata Python-Grundkurs Dezember 2020
# Dozent: Lars Petersen

############################################################
###############  Memory Game  ##############################
############################################################

'''
Tipp von Lars: Kleiner Tipp an die Memory-Projektler: GridLayout mit Buttons, die Buttons per Image ersetzen und auf ButtonKlick austauschen - es gibt aber natürlich auch andere Lösungswege...
'''
### Import von benötigten Modulen ##########################################
from tkinter import *  # GUI-Modul tkinter importieren
import os
from PIL import ImageTk, Image # Modul Pillow
import tkinter.messagebox
import random
import math
import time
import json

########################################################################################################################
###############  Klasse für das Memory-Spielfeld  ######################################################################
########################################################################################################################

class Memory():
  
    def __init__(self, parent):
        '''
        Diese Klasse erzeugt das Memory-Feld mit den einzelnen Karten und liefert die benötigten Methoden.
        '''
        self.parent             = parent # Verweis auf main = Tk()

        self.img_size           = 0     # Variable, in der die je nach Anzahl der Memorykarten errechnete Größe pro Karte zwischenspeichert
        self.img_list           = []    # Liste, in der die geladenen Bilder abgelegt werden
        self.btn_list           = []    # Liste, in der die erzeugten Buttons abgelegt werden, die als Memory-Karten dienen
        self.clicked_btn_list   = []    # Liste, in der quasi die angeklickten Buttons bzw. aufgedeckten Karten zwischengespeichert werden
        self.num_of_cards       = 16    # Standardwert; Variable zum speichern der Anzahl der Memory-Karten; änderung per Regler, siehe Slider/Scale in class Controls
        self.img_id_list        = []    # randomisierte Liste mit Zahlenpaaren, die als Index für die "img_list" dient

        self.s                  = 0     # Variable für Sekunden der Stoppuhr
        self.m                  = 0     # Variable für Minuten der Stoppuhr  
        self.run_clock          = False # Steuervariable für Stoppuhr
        self.time               = "00:00" # Variable als Zwischenspeicher für die Zeit der Stoppuhr
        self.highscore          = {}    # Dict für den Highscore, der als JSON-Datei gespeichert wird 
        self.highscore_txt      = "\n"  # Highscore als String für die Ausgabe im Highscore-Label 

        self.counter            = 0     # Zähler, der alle korrekt aufgedeckten Memory-Paare zählt, 
        self.column             = 0     # Zähler für die Anzahl der Memory-Karten in einer Reihe
        self.row                = 0     # Zähler für die Anzahl der Memory-Karten in einer Spalte

        self.folder             = './bilder/'   # relativer Pfad zu den Bildern
        self.extension          = '.png'        # Dateiformat der Bilder; dient auch der Bereinigung/Filterung der geladenen Elemente aus dem Bilder-Ordner
        
    def initMemoryFrame(self):

        self.loadImgs()                 # Ausführen der Funktion/Methode loadImgs: lädt die Bilder aus dem angegebenen Ordner und passt die Größe entsprechend der Kartenanzahl an 
        self.randomImgIdList()          # Ausführen der Funktion/Methode randomImgIdList: erzeugt Liste mit Zahlenpaaren und mischt diese zufällig
        self.loadHighscore()            # Ausführen der Funktion/Methode loadHighscore: lädt, wenn vorhanden, die Datei memory_highscore.json

        self.frame = Frame(relief = "groove", bd = 2, bg='lightgrey', width = 850, height = 850) # Frame, in dem die Memorykarten als Buttons positioniert werden
        self.frame.grid(column = 1, row = 0, padx = 10, pady = 10)  # Positionierung des Frames im Anwendungsfenster(main)
        #self.frame.grid_propagate(False) # deaktiviert "auto-expand by content" und fixiert die Framegröße anhand der width und height im Frame()
        
        for c in range(self.num_of_cards): # Erzeugung der Buttons als Memory-Karten und Anordnung mit Grid im MemoryGame-Frame
            self.btn = Button(self.frame, text="Memory Card", image = self.main_img, command=lambda btn_id = c: self.onClick(btn_id, self.img_id_list[btn_id])) # Nutzung von anonymer Funktion "lambda" 
            self.btn.grid(column = self.column, row = self.row, padx=3, pady=3)
            self.btn_list.append(self.btn) # Buttons in einer Liste speichern, damit deren Referenz erhalten bleibt
            # Zeilenumbruch mit Modulus
            self.column += 1
            #print((self.column + 1) * (self.img_size))
            if (self.column + 1) * (self.img_size) >= 850:
                self.row += 1
                self.column = 0
        #print(self.btn_list)

    def loadImgs(self):
        file_name_img_list = os.listdir(self.folder) # mit os.listdir alle Elemente aus dem Ordner "Bilder" lesen und in einer Liste ablegen
        #TODO: Fehlermeldeung, wenn zu wenige bilder im Ordner
        for file_name in file_name_img_list: # Liste "file_name_img_list" filtern nach allen Elemente, die auf ".png" enden; andere Elemente werden gelöscht
            if not file_name.endswith(self.extension):
                file_name_img_list.remove(file_name)
                #print ("del",file_name)

        # Berechnung der Bildgröße bei aktueller "num_of_cards", damit alle Karten in den Frame passen
        self.img_size = math.floor(math.sqrt(700_000/self.num_of_cards))
        #print("cards", self.num_of_cards, "img_size",self.img_size)
        img_size_without_frame = self.img_size - 10  # 10 pixel abgezogen wegen Padding

        img = Image.open('./back.png')  # Bilddatei der Standardrückseite der Memory-Karten laden
        img = img.resize((img_size_without_frame, img_size_without_frame), Image.ANTIALIAS) # Bildgröße anpassen und Bild glätten
        self.main_img = ImageTk.PhotoImage(img) # umwandeln des PIL-Image in tkinter.PhotoImage

        for file_name in file_name_img_list: # Bilder erzeugen und in Liste packen (Pfad und file_name zusammenführen)
            img = Image.open(self.folder + file_name)
            img = img.resize((img_size_without_frame, img_size_without_frame), Image.ANTIALIAS)
            photo_img = ImageTk.PhotoImage(img)
            self.img_list.append(photo_img)
        random.shuffle(self.img_list) # Elemente zufällig neu sortieren

    def randomImgIdList(self): # Indizes für die verdeckten Bilder der Memory-Karten vorbereiten; da in Memory Paare genutzt werden, kommt jeder Index zweimal vor 
        a = list(range(int(self.num_of_cards/2)))
        b = list(range(int(self.num_of_cards/2)))
        self.img_id_list = a + b
        random.shuffle(self.img_id_list) # gut durchmischen ;)
        #print("merged:", self.img_id_list, self.num_of_cards)

    def onClick(self, btn_id, img_id): # wird aufgerufen, wenn auf eine Karte geklickt wird
        #print(self.btn_list[btn_id], self.img_list[btn_id])
        #print(btn_id, img_id)
        #print("Klick auf btn mit ID:", btn_id)
        #print(self.btn_list)

        # Bedingung: Wenn die zwei aufgedeckten Karten UNGLEICH sind
        if len(self.clicked_btn_list) == 2 and self.clicked_btn_list[0] != self.clicked_btn_list[1]: 
            #print("Kartenpaar nicht gleich")
            for btn in self.clicked_btn_list:
                # btn[0] = index; btn[1] = element
                #print(btn[0],btn[1])
                self.btn_list[btn[0]]["command"] = btn[1]   # ursprüngliches Command wieder dem Button/Karte hinzufügen, damit er wieder geklickt werden kann
                self.btn_list[btn[0]]["image"] = self.main_img # Karte zudecken/umdrehen
            self.clicked_btn_list = [] # "Zwischenspeicher" für die zwei aufgedeckten Karten leeren
        #print(self.btn_list)

        # Bedingung, um zu verhindern, dass mehr als zwei Karten gleichzeitig aufgedeckt werden
        if len(self.clicked_btn_list) < 2:
            self.btn_list[btn_id]["image"] = self.img_list[img_id] # deckt den angeklickten Button/Memory-Karte auf
            temp_tuple = (btn_id, self.btn_list[btn_id]["command"]) # Tupel mit btn-ID und btn-Command; Tupel, weil Elemente zwigend zusammengehören und nicht verändert werden sollen ;) 
            self.clicked_btn_list.append(temp_tuple) # fügt den angeklickten Button/Memory-Karte (id und ) einer seperaten Liste hinzu
            self.btn_list[btn_id]["command"] = ""               # Command vom angeklickten Button/Karte entfernen, um zweiten Klick auf selben Button/Karte zu verhindern
            print("clicked:",len(self.clicked_btn_list))
            # Stoppuhr starten, wenn erste Karte umgedreht/geklickt wird; kein erneuter Start bei weitere Karte
            if not self.run_clock:      # wenn die Stoppuhr nicht läuft, dann Stoppuhr starten;
                self.run_clock = True   # Steuervariable für Stoppuhr geändert
                self.clockCounter()     # Stoppuhr-Funktion aufrufen
        
        # Bedingung: Wenn die zwei aufgedeckten Karten GLEICH sind
        if len(self.clicked_btn_list) == 2:
            clicked_btn_1_id = self.clicked_btn_list[0][0] # zum besseren Verständnis den Wert "ID" der 1. angeklickten Karte an eine Variable mit verständlichem Namen übergeben
            clicked_btn_2_id = self.clicked_btn_list[1][0] # zum besseren Verständnis den Wert "ID" der 2. angeklickten Karte an eine Variable mit verständlichem Namen übergeben
            if self.btn_list[clicked_btn_1_id]["image"] == self.btn_list[clicked_btn_2_id]["image"]: # Prüfung, ob die zwei aufgedeckten Karten identisch/gleich sind
                #print("ist gleich")
                self.counter += 2          # gleiche Karten/Karten-Paare zählen
                self.clicked_btn_list = [] # Liste wird für die nächsten zwei Karten geleert
        
        if self.counter == len(self.btn_list): # sorgt dafür, dass die Stoppuhr angehalten wird, sobald alle Memory-Paare aufgedeckt wurden
            self.run_clock = False  # Steuervariable für Stoppuhr geändert
            self.saveHighscore()    # Zeit im Highscore/JSON-Datei speichern
            self.parent.after(100, self.showInfo) # Info ausgeben, dass Spiel erfolgreich beendet
        #print("b",len(self.clicked_btn_list), self.clicked_btn_list)

    def showInfo(self): # Messagebox am Ende des Spiels
        tkinter.messagebox.showinfo("Glückwunsch!", "Du hast alle Paare gefunden.\nDu hast {} benötigt.".format(self.time))

    def loadHighscore(self): # Highscore/JSON-Datei laden, wenn vorhanden, und Inhalt aufbereiten für Ausgabe in Tkinter-Label
        try: # um Programmabbruch zu vermeiden, try-except angewendet
            self.highscore_txt = "\n"   # Zeilenumbruch zu beginn der Ausgabe (Formatierung)
            with open("memory_highscore.json") as jsonfile: # Datei öffen, lesen und anschließend schließen
                data = json.load(jsonfile) # Umwandlung der JSON-Datei und übergabe an Dictionary "self.highscore"
            #print(data)
            self.highscore = data
            for karten in self.highscore: # Dict durchlaufen und formatiert in String legen für Ausgabe in Label
                self.highscore[karten].sort() # Zeiten aufsteigend sortieren
                while len(self.highscore[karten]) > 3: # kürzt die Listen mit den Zeiten auf 3 Einträge pro key/Kartenanzahl
                    self.highscore[karten].pop()
                #print(karten, self.highscore[karten].sort())
                for zeit in self.highscore[karten]:
                    self.highscore_txt += "{} Karten : {}\n".format(karten, zeit) # 
                    #print(karten, zeit)
                self.highscore_txt += "\n"
            #print(self.highscore)
        except:
            print("Keine Datei vorhanden oder Datei konnte nicht geladen werden.") # Info: Entwickler ;)

    def saveHighscore(self):
        # Kartenanzahl und benötigte Zeit in einem dict/json
        key = str(self.num_of_cards) # Kartenanzahl als Key für highscore-Dictionary; key muss String sein
        if key in self.highscore:    # Prüfen, ob im highscore-Dictionary bereits ein entsprechender Key vorhanden ist
            #print("key is in (true)")
            self.highscore[key].append(self.time) # wenn True, dann gestoppte Zeit der Liste mit dem entsprechenden Key hinzufügen
        else:
            time_list = []                  # wenn False, dann leere Liste erzeugen ...
            time_list.append(self.time)     # ... gestoppte Zeit der Liste hinzufügen
            self.highscore[key] = time_list # Liste mit dem entsprechenden Key zum highscore-Dictionary hinzufügen

        #print(self.highscore, type(self.highscore))
        
        try: # um Programmabbruch zu vermeiden, try-except angewendet
            with open("memory_highscore.json", "w") as data: # vorhandene Datei öffnen; falls keine Datei vorhanden, dann wird sie automatisch erstellt
                json.dump(self.highscore, data, indent = 4) # Umwandlung des Dictionary "self.highscore" in JSON-Format und ablegen der Daten in der Datei; "ident = 4" sorgt für die mehrzeilige Formatierung der JSON-Datei    
            print("Highscore wurde erfolgreich gespeichert") # Info: Entwickler ;)
        except Exception as ex:
            print ("Fehler:", ex, "- Highscore wurde nicht gespeichert") # Info: Entwickler ;)
        self.loadHighscore() # mit dem Speichern erfolgt auch ein Laden der gerade gespeicherten Datei, um die Highscore-Ausgabe im Label zu aktualisieren

    def clockCounter(self): # Stoppuhr
        if self.run_clock:
            self.s += 1
            self.time = "{:02d}:{:02d}".format(self.m, self.s)
            #print(self.time)

        if self.s == 59:
            self.m += 1
            self.s = -1
        
        if self.m == 60 and self.s == 0:
            self.time = "99:99"
            self.run_clock = False
            
        if self.run_clock: # die Stoppuhr-Funktion ruft sich solange selbst auf, bis die Steuervariable "run_clock" auf False gesetzt wird
            self.parent.after(1000, self.clockCounter) # Wiederholung alle 1_000 Millisekunden = 1 Sekunde

    def frameQuit(self):       # Memory-Frame auflösen und die Uhr anhalten
        self.run_clock = False 
        self.frame.destroy()



########################################################################################################################
###############  Klasse für den Footer  ################################################################################
########################################################################################################################

class Bottom_frame(): 
    def __init__(self, parent):
        '''
        Diese einfache Klasse "Bottom_frame" erzeugt einen Frame mit Informationen in einem Label.
        '''
        self.parent = parent
        self.frame = Frame(self.parent, bg = 'lightgrey')  #, relief = "groove", bd =2)
        self.frame.grid(column = 0, row = 1, columnspan = 2, ipadx = 5, ipady = 5, sticky = NSEW)

        # self.lable = Label(self.frame, text = "Projektarbeit: Ben Borchers – cimdata Python-Grundkurs – Dozent: Lars Petersen – Dezember 2020", bg='lightgrey')
        # self.lable.pack(expand=1)
        self.frame.columnconfigure(0, weight = 1)
        self.frame.columnconfigure(1, weight = 1)
        self.frame.columnconfigure(2, weight = 1)
        self.frame.columnconfigure(3, weight = 1)

        self.lable1 = Label(self.frame, text = "Projektarbeit: Ben Borchers", fg = 'darkgrey', bg='lightgrey')
        self.lable1.grid(column = 0, row = 0, ipadx = 5, ipady = 5)      
        self.lable2 = Label(self.frame, text = "cimdata Python-Grundkurs", fg = 'darkgrey', bg='lightgrey')
        self.lable2.grid(column = 1, row = 0, ipadx = 5, ipady = 5)
        self.lable3 = Label(self.frame, text = "Dozent: Lars Petersen", fg = 'darkgrey', bg='lightgrey')
        self.lable3.grid(column = 2, row = 0, ipadx = 5, ipady = 5)
        self.lable4 = Label(self.frame, text = "Dezember 2020", fg = 'darkgrey', bg='lightgrey')
        self.lable4.grid(column = 3, row = 0, ipadx = 5, ipady = 5)



########################################################################################################################
###############  Klasse die Steuerelemente  ############################################################################
########################################################################################################################

class Controls():
    def __init__(self, parent, app):
        '''
        Die Klasse "Controls" erzeugt einen Frame mit Steuerungselementen und Anzeigen zur Spielkontrolle.
        '''
        self.parent = parent
        self.app = app          # über Parameter Zugriff auf Objekt "game = Memory(main)"
        self.frame = Frame(self.parent, bg = 'lightgrey') #, relief = "groove", bd = 2)
        self.frame.columnconfigure(0, weight = 1)   # Spalte 1 und 2 des grids haben gleiche Breite (relativ)
        self.frame.columnconfigure(1, weight = 1)   # Spalte 1 und 2 des grids haben gleiche Breite (relativ)
        
        self.frame.grid(column = 0, row = 0, padx = (15, 0), pady= 10, sticky = N)
        
        self.headline = Label(self.frame, text = "cimdata", font='Helvetica 24 bold', fg = 'darkred', bg = 'lightgrey')
        self.headline.grid(column = 0, row = 0, columnspan = 2, padx=4, pady = (30, 0))
        self.sub_headline = Label(self.frame, text = "Python-Memory", font='Helvetica 18 bold', bg = 'lightgrey')
        self.sub_headline.grid(column = 0, row = 1, columnspan = 2, padx=4, pady = (0, 30))
        
        self.slider_selected = IntVar() # aktualiesiert automatisch den gewälten Wert des Sliders
        self.slider_selected.set(self.app.num_of_cards) # setzt den Slider auf einen vorausgewählten Wert, in diesem Fall aus dem Objekt "game = Memory(main)"

        self.slider_label = Label(self.frame, text = "Anzahl der Memory-Karten", bg = 'lightgrey')
        self.slider_label.grid(column = 0, row = 2, columnspan = 2, padx=4, pady=(20,0))
        self.slider = Scale(self.frame, from_= 4, to = 64, orient = HORIZONTAL, bg= 'lightgrey', length = 180, tickinterval = 60, variable = self.slider_selected, command = self.valueCheck)
        self.slider.grid(column = 0, row = 3, columnspan = 2, padx=4, pady=(0, 20))
        
        self.restart_btn = Button(self.frame, text = "Neustart", width = 20, highlightbackground="lightgrey", command = self.restart)
        self.restart_btn.grid(column = 0, row = 4, columnspan = 2, padx=4, pady=(15, 30))

        self.clock_label = Label(self.frame, text = "Zeit:", bg = 'lightgrey')
        self.clock_label.grid(column = 0, row = 5, padx=1, pady=25, sticky = E)
        self.clock = Label(self.frame, text = "00:00", borderwidth=2, relief="ridge")
        self.clock.grid(column = 1, row = 5, padx=1, pady=25, sticky = W)

        self.highscore_label = Label(self.frame, text = "Highscore", bg = 'lightgrey')
        self.highscore_label.grid(column = 0, row = 6, columnspan = 2, padx=4, pady = 0)
        self.highscore_out = Label(self.frame, text = "Hier geht gleich die Post ab!", borderwidth = 2, relief="ridge", width = 20, height = 17)
        self.highscore_out.grid(column = 0, row = 7, columnspan = 2, padx=4, pady = (0, 10))

        self.cancel_game_btn = Button(self.frame, text = "Spiel beenden", width = 20, highlightbackground="lightgrey", command = self.quitMain)
        self.cancel_game_btn.grid(column = 0, row = 8, columnspan = 2, padx=4, pady=20)

        self.app.initMemoryFrame()
        self.updateTime() # startet die permanente Abfrage der Stoppuhr aus Objekt "game = Memory(main)" und die permanente Ausgabe des Highscore-Textes im Label :/

    # folgende Funktion gefunden auf: https://stackoverflow.com/questions/25745011/how-to-use-tkinter-slider-scale-widget-with-discrete-steps
    # und auf meine Bedürfnisse leicht angepasst 
    def valueCheck(self, value): # nur bestimmte Werte des Sliders(Scale) zulassen
        #print(value)
        value_list = [4,16,36,64]
        new_value = min(value_list, key=lambda x:abs(x-float(value)))
        self.slider.set(new_value)
        self.restart()
        
    def updateTime(self): # permanente Abfrage der Stoppuhr aus Objekt "game = Memory(main)" und die permanente Ausgabe des Highscore-Textes im Label :/
        #print("update:", self.clock["text"])
        self.clock["text"] = self.app.time
        if self.app.time == "99:99":
            self.clock["fg"] = "red"
        self.highscore_out["text"] = self.app.highscore_txt
        self.parent.after(499, self.updateTime)

    def restart(self): # startet den MemoryFrame neu
        if not self.app.run_clock: # wenn die Stoppuhr nicht läuft (noch kein Spiel begonnen oder alle Memory-Karten aufgedeckt), dann Neustart ohne Sicherheitsabfrage
            self.app.frameQuit() # Memory-Frame auflösen
            self.app = Memory(self.parent) # Memory-Frame neu laden     
            self.app.num_of_cards = self.slider.get()
            #print(type(self.app.num_of_cards))
            self.app.initMemoryFrame()
        else:
            meldung = tkinter.messagebox.askyesno("Neustart", "Willst Du das aktuelle Spiel wirklich beenden?")
            if meldung:
                self.app.frameQuit() # Memory-Frame auflösen
                self.app = Memory(self.parent) # Memory-Frame neu laden
                self.app.num_of_cards = self.slider.get()
                self.app.initMemoryFrame()

    def quitMain(self): # Beendet das gesamte Programm
        meldung = tkinter.messagebox.askyesno("Programm schließen", "Willst Du das Programm wirklich beenden?")
        if meldung:
            self.parent.destroy()


########################################################################################################################
############### tkinter-Fenster initiieren ##############################################################################
########################################################################################################################

def app(): # TODO erklären was es tut
    main = Tk()
    main.title("cimdata Python-Memory") # Fenstertitel vergeben
    main.geometry("1100x900+100+100") # Fenstergröße festgelegt 
    main.resizable(False, False) # Fenster ist nicht skalierbar
    main.configure(bg='lightgrey')
    main.columnconfigure(0, weight = 1) # gleichmäßige Breite der grid-Zellen (relativ)
    main.columnconfigure(1, weight = 1)
    main.rowconfigure(0, weight = 1)
    main.rowconfigure(1, weight = 1)

    game = Memory(main) # Objekt aus Klasse Memory erzeugen usw.
    control_panel = Controls(main, game)
    footer = Bottom_frame(main)

    main.mainloop() # Endlosschleife hält das Programm am Laufen, bis es beendet wird

if __name__ == '__main__': # verhindert Ausführung des Skripts bei Nutzung der Datei als Library/Modul
    app() 
    


